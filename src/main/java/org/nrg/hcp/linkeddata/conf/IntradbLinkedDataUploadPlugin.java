package org.nrg.hcp.linkeddata.conf;

import org.apache.log4j.Logger;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(
			value = "intradbLinkedDataUploadPlugin",
			name = "Intradb Linked Data Upload Plugin"
		)
@ComponentScan({ 
	"org.nrg.hcp.linkeddata.components"
	})
public class IntradbLinkedDataUploadPlugin {
	
	/** The logger. */
	public static Logger logger = Logger.getLogger(IntradbLinkedDataUploadPlugin.class);

	/**
	 * Instantiates a new ccf subject ids plugin.
	 */
	public IntradbLinkedDataUploadPlugin() {
		logger.info("Configuring IntraDB linked data upload plugin");
	}
	
}
